import React from "react";
import { useGetThingsQuery } from "./services/things";
import ThingItem from "./ThingItem";

const Things = () => {
    const { data, isLoading } = useGetThingsQuery()
    if (isLoading) return <div>Loading...</div>
    if (data.length === 0) return <div>No things :(</div>
    return (
        <ul>
            {data.map(thing => <ThingItem key={thing.id} {...thing} />)}
        </ul>
    )
}


export default Things;
