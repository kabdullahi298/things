from main import app
from fastapi.testclient import TestClient
from queries.things import ThingsQueries
from models import Thing, ThingParams

client = TestClient(app)


class ThingQueriesMock:
    def create(self, params: ThingParams) -> Thing:
        thing = params.dict()
        thing['id'] = '1337'
        return Thing(**thing)

    def get_all(self) -> list[Thing]:
        return [Thing(id='1337', name='Thing 1')]

    def delete(self, _id: str) -> bool:
        return True


def test_create_thing():
    app.dependency_overrides[ThingsQueries] = ThingQueriesMock
    thing = {
        'name': 'Thing 1'
    }
    res = client.post('/api/things', json=thing)
    assert res.json()['id'] == '1337'
    app.dependency_overrides = {}


def test_get_all_things():
    app.dependency_overrides[ThingsQueries] = ThingQueriesMock
    res = client.get('/api/things')
    assert len(res.json()['things']) == 1
    app.dependency_overrides = {}


def test_delete_thing():
    app.dependency_overrides[ThingsQueries] = ThingQueriesMock
    res = client.delete('/api/things/1')
    assert res.json()
    app.dependency_overrides = {}
